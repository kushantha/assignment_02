#include <stdio.h>
#include <stdlib.h>

int main()
{
    float r,a;
    const pi = 3.14;
    printf("Enter radius = ");
    scanf("%f", &r);
    a = pi * r * r;
    printf("Area of the circle = %f " ,a);
    return 0;
}
